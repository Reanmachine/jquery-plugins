(function($) {
    
    var Loaders = {};
    
    $.registerAutoloader = function(selector, callback) {
        Loaders[selector] = callback;
    };
    
    $.fn.autoload = function(matchThis) {
        var match = this;
        if (!matchThis) {
            match = this.find('.auto-load');
        }
        
        for(var selector in Loaders) {
            var method = Loaders[selector];
            if (typeof method != "function") continue;
            
            match.filter(selector).each(function(index) {
                var $this = $(this);
                if ($this.data('initialized')) return;
                $this.data('initialized', true);
                
                method.apply($this, [index]);
            });
        }
    };
    
    var windowLoaded = false;
    var autoloadOnWindowLoaded = false;
    
    $.autoload = function(selector, matchThis) {
        
        if (!matchThis) { matchThis = false; }
        
        if (selector && (selector instanceof jQuery)) {
            selector.autoload(matchThis);
            return;
        }
                
        if (selector && (typeof selector == "string")) {
            $(selector).autoload(matchThis);
            return;
        }
        
        if (windowLoaded) {
            autoloadAll();
        } else {
            autoloadOnWindowLoaded = true;
        }
    };
        
    function autoloadAll() {
        $('.auto-load').autoload(true);
    }
        
    $(window).load(function() {
        windowLoaded = true;
        if (autoloadOnWindowLoaded) {
            autoloadAll();
        }
    });
    
})(jQuery);